* El proyecto está hecho en Django con Python 3.8
para ejecutarlo deben instalar y crear un entorno virtual(virtualenv), 
yo recomiendo pyenv.

* una vez creado el virtualenv, instalar todas las dependencias que estan
en el archivo requirements.txt

* crear una base de datos mongo y colocarla en los settings del proyecto

* crear un superuser con el comando python manage.py createsuperuser y seguir los pasos


* luego ejecutar el comando python managepy runserver para ejecutar el servidor local y empezar a probar