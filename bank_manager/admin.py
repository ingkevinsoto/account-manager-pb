from django.contrib import admin

# Register your models here.
from bank_manager.models import Account


class AccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'balance', 'user')


admin.site.register(Account, AccountAdmin)
