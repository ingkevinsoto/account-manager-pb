from django.urls import path
from bank_manager import views

urlpatterns = [
    path('account-data/', views.AccountView.as_view()),
    path('upload/(?P<filename>[^/]+)$', views.FileUploadView.as_view())

]
