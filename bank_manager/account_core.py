from idlelib.idle_test.test_config import usercfg

from django.contrib.auth.models import User

from bank_manager.models import Account
#import requests


class AccountCore(object):
    def __init__(self, account_step_file: dict):
        self.account_step_file = {
  "steps": [
    {
      "id": "validate_account",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"},
        "pin": {"from_id": "start", "param_id": "pin"}
      },
      "action": "validate_account",
      "transitions": [
        {
          "condition": [
            {"from_id": "validate_account", "field_id": "is_valid", "operator": "eq", "value": True}
          ],
          "target": "account_balance"
        }
      ]
    },
    {
      "id": "account_balance",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"}
      },
      "action": "get_account_balance",
      "transitions": [
        {
          "condition": [
            {"from_id": "account_balance", "field_id": "balance", "operator": "gt", "value": 100000},
          ],
          "target": "withdraw_30"
        },
        {
          "condition": [
            {"from_id": "account_balance", "field_id": "balance", "operator": "lt", "value": 100000},
          ],
          "target": "deposit_200"
        }
      ]
    },
    {
      "id": "withdraw_30",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"},
        "money": {"from_id": None, "value": 30}
      },
      "action": "withdraw_in_dollars",
      "transitions": [
        {
          "condition": [],
          "target": "account_balance_end_30"
        }
      ]
    },
    {
      "id": "account_balance_end_30",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"}
      },
      "action": "get_account_balance",
      "transitions": []
    },
    {
      "id": "deposit_200",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"},
        "money": {"from_id": None, "value": 200000}
      },
      "action": "deposit_money",
      "transitions": [
        {
          "condition": [],
          "target": "account_balance_200"
        }
      ]
    },
    {
      "id": "account_balance_200",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"}
      },
      "action": "get_account_balance",
      "transitions": [
        {
          "condition": [
            {"from_id": "account_balance", "field_id": "balance", "operator": "gt", "value": 250000},
          ],
          "target": "withdraw_50"
        }
      ]
    },
    {
      "id": "withdraw_50",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"},
        "money": {"from_id": None, "value": 50000}
      },
      "action": "withdraw_in_dollars",
      "transitions": [
        {
          "condition": [],
          "target": "account_balance_end_50"
        }
      ]
    },
    {
      "id": "account_balance_end_50",
      "params": {
        "user_id": {"from_id": "start", "param_id": "user_id"}
      },
      "action": "get_account_balance",
      "transitions": []
    },
  ],
  "trigger": {
    "params": {
        "user_id": "kevin",
        "pin": 2090
    },
    "transitions": [
      {
        "target": "validate_account",
        "condition": []
      }
    ],
    "id": "start"
  }
}
        self.step_dict = {}

    def validate_account(self, *args, **kwargs):
        user = User.objects.get(username=kwargs.get('params').get('user_id'))
        return {
            'is_valid': user #and user.pin == kwargs.get('user_pin', {})
        }

    def get_account_balance(self, *args, **kwargs):
        account = Account.objects.get(user__username=kwargs.get('params').get('user_id'))
        step_dict = {'balance': account.balance}
        step_account_balance = self.get_step('account_balance')
        step_account_balance['values'] = step_dict
        return step_dict

    @classmethod
    def withdraw(cls, *args, **kwargs):
        account = Account.objects.get(user__username=kwargs.get('params').get('user_id'))
        account.balance -= kwargs.get('params').get('money')
        account.save()
        return {}

    @classmethod
    def withdraw_in_dollars(cls, *args, **kwargs):
        # requests.get('https://trm-colombia.vercel.app/?date=2018-12-31')
        trm = 3200
        account = Account.objects.get(user__username=kwargs.get('params').get('user_id'))
        account.balance -= kwargs.get('params').get('money')*trm
        account.save()
        return {}

    @classmethod
    def deposit_money(cls, *args, **kwargs):
        account = Account.objects.get(user__username=kwargs.get('params').get('user_id'))
        account.balance += kwargs.get('params').get('money')
        account.save()
        return {}

    @classmethod
    def evaluate_condition(cls, operator, value_from_file, field_value):
        if operator == 'eq':
            return field_value == value_from_file
        elif operator == 'gt':
            return field_value > value_from_file
        elif operator == 'gte':
            return field_value >= value_from_file
        elif operator == 'lt':
            return field_value < value_from_file
        elif operator == 'lte':
            return field_value <= value_from_file

    def get_step(self, step_id):
        for step in self.account_step_file.get('steps'):
            if step_id == step.get('id'):
                return step

    def get_params(self, step):
        params_dict = step.get('params')
        dict_response = {}
        for key, value in params_dict.items():
            from_id = value.get('from_id')
            param_id = value.get('param_id')
            if from_id == 'start' and self.account_step_file.get('trigger').get('id') == 'start':
                dict_response[key] = self.account_step_file.get('trigger').get('params').get(param_id)
            else:
                if not param_id:
                    dict_response[key] = value.get('value')

        return dict_response

    def execute_action(self, action: str, *args, **kwargs):
        print("******", action, "********")
        kwargs['step']['values'] = getattr(self, action)(*args, **kwargs)

    def execute_transaction(self, step):
        for transaction in step['transactions']:
            self.execute_action(step.get('action'), step=step, params=self.get_params(step))

    def run(self):
        if self.account_step_file.get('trigger', {}) and self.account_step_file.get('steps', {}):
            for step in self.account_step_file.get('steps'):
                self.execute_action(step.get('action'), step=step, params=self.get_params(step))


