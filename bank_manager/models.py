from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Account(models.Model):
    balance = models.FloatField()
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_valid = models.BooleanField(default=False)

    def __unicode__(self):
        return u'{}'.format(self.balance)

    def __str__(self):
        return u'{}'.format(self.balance)
